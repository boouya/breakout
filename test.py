import pygame as pg

import random
import math
from Game import Game
from constants import *
from LevelEditor import LevelEditor


pg.init()


lEdi = Game("BreakOut!")


while lEdi.getState() != GAME_STOP:

    lEdi.processInputs()

    lEdi.update_game()
    